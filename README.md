This repository is on processing the debate protocols from German Bundestag. We are storing the results at [GitHub](https://github.com/mathias-goebel/plenarsitzungsprotokolle/).

## How it works
It gets a list of files provided via the so called “Open Data” interface to the Bundestag. The interace itself is not very handy. No OpenAPI description is provided, the list is integrated in the website via AJAX…

If you think one of these flaws got some attention, pls open a ticket in [this repository](https://gitlab.gwdg.de/mgoebel/plenarsitzungen) or write an email to gitlab+mgoebel-plenarsitzungen-4972-issue-@gwdg.de.

Written in XQuery, running on eXist-db.